const express = require('express');
const fs = require('fs')
const app = express();
const emailValidator = require("email-validator");

app.use(express.static(__dirname + '/public/'));

// funcion main
const magic = data => {
  //cortando csv por linea
  const porLinea = data.split(/\n/g);
  let arrPadre = []
  //cortando lineas por ;
  porLinea.forEach((linea,i) => {
    const arrHijo = linea.split(';')
    //comprobacion de cliente y push del index del csv original
    if(arrHijo[2] == 'CLIENTE'){
      arrHijo.push(i)
      arrPadre.push(arrHijo)
    }  
  });
  
  // comprobaciones
  // estas son las que solo hacen logs de errores
  lengthVer(arrPadre)
  verificarEmail(arrPadre)
  // esta es la unica por ahora que devulve un array distinto 
  arrPadre = verificarPhone(arrPadre) 

}
//esto se entiende bastante
const lengthVer = arrPadre=>{
  arrPadre.forEach((linea,i)=>{
    if(linea.length!=9){
      fs.appendFileSync('errores-lineas.txt', 'error cantidad de lineas en '+ linea[linea.length -1] + "\n", (err) => {
        if (err) {
          console.log('error, ' + err);
        }}
      );
    }
  })
}

// aca uso una libreria anda bastantew bien
const verificarEmail = arrPadre =>{
  arrPadre.forEach((linea,i)=>{
    if (emailValidator.validate(linea[3])) {
    } else {
      fs.appendFileSync('errores emails.txt', 'ERROR!! Email validation  fail error en index '+ linea[8] + "\n", (err) => {
        if (err) {
          console.log('error, see the logs file to more information');
        }}
      );
    }
  })
}


const verificarPhone = arrPadre => {
  let espana = 0
  let venezuela = 0
  let argentina = 0
  let other = 0
  arrPadre.forEach((linea,i )=>{
    switch (linea[5]) {
      case 'Espa�a':
        espana++
        arrPadre[i][4] = comporbarTelefonoEspana(linea[4])
        break;
      case 'venezuela':
        venezuela++
        break;
      case 'Argentina':
        argentina++
        break;
      default:
        other++
        break;
    }

  })
  fs.appendFileSync('contadores de países.txt', 'españa = '+ espana + "\n", (err) => {
    if (err) {
      console.log('error, see the logs file to more information');
    }}
  );
  fs.appendFileSync('contadores de países.txt', 'argentina = '+ argentina + "\n", (err) => {
    if (err) {
      console.log('error, see the logs file to more information');
    }}
  );
  fs.appendFileSync('contadores de países.txt', 'venezuela = '+ venezuela + "\n", (err) => {
    if (err) {
      console.log('error, see the logs file to more information');
    }}
  );
  fs.appendFileSync('contadores de países.txt', 'otros = '+ other + "\n", (err) => {
    if (err) {
      console.log('error, see the logs file to more information');
    }}
  ); 
  return(arrPadre)
}

const comporbarTelefonoEspana= telefono =>{
  let telefonoFixed = telefono.replace(/ /g, "")
  if(telefonoFixed.length == 12 && telefonoFixed[0]=="+" && telefonoFixed[1]=="3" && telefonoFixed[2]=="4"){
    return(telefonoFixed)
  }else if(telefonoFixed.length == 9 && (telefonoFixed[0]==9 || telefonoFixed[0]==6)){
    telefonoFixed = '+34' + telefonoFixed;
    return(telefonoFixed)
  }
}



fs.readFile('./master-clientes.txt','utf-8',(err, data) => {
  if(err) {
    console.log('error: ', err);
  } else {
    magic(data);
  }
});

app.listen('3000', function() {
  console.log('Servidor web escuchando en el puerto 3000');
});